package com.hawker.api.controllers;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.AlreadyExistException;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.models.Page;
import com.hawker.api.resources.BusinessResource;
import com.hawker.api.resources.ItemResource;
import com.hawker.api.services.BusinessService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class UserController {

    private final BusinessService businessService;

    public UserController(BusinessService businessService) {
        this.businessService = businessService;
    }

    @RequestMapping(method = GET, value = "/u/businesses")
    public ResponseEntity<ApiResponse> getSelfBusinesses(@RequestParam(name = "page", defaultValue = "1", required = false) int page,
                                                         @RequestParam(name = "count", defaultValue = "5", required = false) int count,
                                                         HttpServletRequest request)
            throws InvalidParameterException, AlreadyExistException, NotFoundException {
        User user = (User) request.getAttribute("principal");
        Page<BusinessResource> paginatedBusinesses = businessService.getPaginatedBusinesses(user, page, count);
        return new ResponseEntity<>(new ApiResponse(paginatedBusinesses, success), HttpStatus.OK);
    }
}
