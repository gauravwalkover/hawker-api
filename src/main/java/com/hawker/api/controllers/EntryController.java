package com.hawker.api.controllers;

import com.hawker.api.dao.models.Account;
import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.resources.EntryResource;
import com.hawker.api.services.AccountService;
import com.hawker.api.services.BusinessService;
import com.hawker.api.services.EntryService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
public class EntryController {

    private final BusinessService businessService;
    private final AccountService accountService;
    private final EntryService entryService;

    public EntryController(BusinessService businessService, AccountService accountService, EntryService entryService) {
        this.businessService = businessService;
        this.accountService = accountService;
        this.entryService = entryService;
    }

    @RequestMapping(method = POST, value = "/business/{businessUniqueName:.+}/accounts/{accountUniqueName:.+}/entries")
    public ResponseEntity<ApiResponse> createEntry(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                   @PathVariable(value = "accountUniqueName") String accountUniqueName,
                                                   @RequestBody EntryResource entryResource, HttpServletRequest request)
            throws InvalidParameterException, NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        Account account = accountService.getAccountByUniqueNameAndBusiness(accountUniqueName, business);
        EntryResource createdEntry = entryService.createEntry(entryResource, account, business, user);
        return new ResponseEntity<>(new ApiResponse(createdEntry, success), HttpStatus.CREATED);
    }

    @RequestMapping(method = GET, value = "/business/{businessUniqueName:.+}/entries/{entryUniqueName:.+}")
    public ResponseEntity<ApiResponse> getEntry(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                @PathVariable(value = "entryUniqueName") String entryUniqueName,
                                                HttpServletRequest request) throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        EntryResource entry = entryService.getEntry(entryUniqueName, business);
        return new ResponseEntity<>(new ApiResponse(entry, success), HttpStatus.OK);
    }

    @RequestMapping(method = DELETE, value = "/business/{businessUniqueName:.+}/entries/{entryUniqueName:.+}")
    public ResponseEntity<ApiResponse> deleteEntry(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                   @PathVariable(value = "entryUniqueName") String entryUniqueName,
                                                   HttpServletRequest request) throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        entryService.deleteEntry(entryUniqueName, business);
        return new ResponseEntity<>(new ApiResponse("Entry deleted.", success), HttpStatus.OK);
    }

    @RequestMapping(method = PATCH, value = "/business/{businessUniqueName:.+}/entries/{entryUniqueName:.+}")
    public ResponseEntity<ApiResponse> updateEntryPatch(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                        @PathVariable(value = "entryUniqueName") String entryUniqueName,
                                                        @RequestBody EntryResource entryResource,
                                                        HttpServletRequest request) throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        EntryResource entry = entryService.getEntry(entryUniqueName, business);
        EntryResource updatedEntry = entryService.updateEntryPatch(entry.getModel(), entryResource, user);
        return new ResponseEntity<>(new ApiResponse(updatedEntry, success), HttpStatus.OK);
    }

}
