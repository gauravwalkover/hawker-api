package com.hawker.api.controllers;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.exceptions.UnexpectedException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.models.AccountBalanceReport;
import com.hawker.api.models.Page;
import com.hawker.api.services.BusinessService;
import com.hawker.api.services.ReportService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import java.util.List;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class ReportController {

    private final BusinessService businessService;
    private final ReportService reportService;

    public ReportController(BusinessService businessService, ReportService reportService) {
        this.businessService = businessService;
        this.reportService = reportService;
    }

    @RequestMapping(method = GET, value = "/business/{businessUniqueName:.+}/account-balances")
    public ResponseEntity<ApiResponse> generateAccountBalanceReports(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                                     @RequestParam(name = "page", defaultValue = "1", required = false) int page,
                                                                     @RequestParam(name = "count", defaultValue = "5", required = false) int count,
                                                                     @RequestParam(name = "paymentStatus") String paymentStatus,
                                                                     @RequestParam(name = "type") String type,
                                                                     @RequestParam(name = "from", required = false) String from,
                                                                     @RequestParam(name = "to", required = false) String to,
                                                                     HttpServletRequest request)
            throws InvalidParameterException, NotFoundException, UnexpectedException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        Page<AccountBalanceReport> paginatedAccountBalanceReports = reportService
                .generatePaginatedAccountBalanceReport(business, paymentStatus, from, to, page, count);
        return new ResponseEntity<>(new ApiResponse(paginatedAccountBalanceReports, success), HttpStatus.OK);
    }
}
