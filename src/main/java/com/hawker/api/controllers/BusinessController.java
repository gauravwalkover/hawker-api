package com.hawker.api.controllers;

import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.AlreadyExistException;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.resources.BusinessResource;
import com.hawker.api.services.BusinessService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class BusinessController {

    private final BusinessService businessService;

    public BusinessController(BusinessService businessService) {
        this.businessService = businessService;
    }

    @RequestMapping(method = POST, value = "/business")
    public ResponseEntity<ApiResponse> createBusiness(@RequestBody BusinessResource businessResource, HttpServletRequest request)
            throws InvalidParameterException, AlreadyExistException, NotFoundException {
        User user = (User) request.getAttribute("principal");
        BusinessResource createdBusiness = businessService.createBusiness(businessResource, user);
        return new ResponseEntity<>(new ApiResponse(createdBusiness, success), HttpStatus.CREATED);
    }

    @RequestMapping(method = GET, value = "/business/{businessUniqueName:.+}")
    public ResponseEntity<ApiResponse> getBusiness(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                   HttpServletRequest request)
            throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        BusinessResource business = businessService.getBusiness(businessUniqueName, user);
        return new ResponseEntity<>(new ApiResponse(business, success), HttpStatus.OK);
    }

}
