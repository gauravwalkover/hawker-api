package com.hawker.api.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.AlreadyExistException;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.models.Page;
import com.hawker.api.resources.ItemResource;
import com.hawker.api.services.BusinessService;
import com.hawker.api.services.ItemService;
import com.hawker.api.utils.JsonViews;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
public class ItemController {

    private final BusinessService businessService;
    private final ItemService itemService;

    public ItemController(BusinessService businessService, ItemService itemService) {
        this.businessService = businessService;
        this.itemService = itemService;
    }

    @RequestMapping(method = POST, value = "/business/{businessUniqueName:.+}/items")
    public ResponseEntity<ApiResponse> createItem(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                  @RequestBody ItemResource itemResource,
                                                  HttpServletRequest request)
            throws InvalidParameterException, NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        ItemResource createdItem = itemService.createItem(itemResource, business, user);
        return new ResponseEntity<>(new ApiResponse(createdItem, success), HttpStatus.CREATED);
    }

    @RequestMapping(method = GET, value = "/business/{businessUniqueName:.+}/items/{itemUniqueName:.+}")
    public ResponseEntity<ApiResponse> getItem(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                               @PathVariable(value = "itemUniqueName") String itemUniqueName,
                                               HttpServletRequest request)
            throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        ItemResource item = itemService.getItem(itemUniqueName, business);
        return new ResponseEntity<>(new ApiResponse(item, success), HttpStatus.OK);
    }

    @RequestMapping(method = DELETE, value = "/business/{businessUniqueName:.+}/items/{itemUniqueName:.+}")
    public ResponseEntity<ApiResponse> deleteItem(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                  @PathVariable(value = "itemUniqueName") String itemUniqueName,
                                                  HttpServletRequest request)
            throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        itemService.deleteItem(itemUniqueName, business);
        return new ResponseEntity<>(new ApiResponse("Item deleted.", success), HttpStatus.OK);
    }

    @RequestMapping(method = GET, value = "/business/{businessUniqueName:.+}/items")
    public ResponseEntity<ApiResponse> getItems(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                @RequestParam(name = "page", defaultValue = "1", required = false) int page,
                                                @RequestParam(name = "count", defaultValue = "5", required = false) int count,
                                                HttpServletRequest request) throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        Page<ItemResource> paginatedItems = itemService.getPaginatedItems(business, page, count);
        return new ResponseEntity<>(new ApiResponse(paginatedItems, success), HttpStatus.OK);
    }
}
