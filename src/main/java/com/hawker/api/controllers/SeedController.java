package com.hawker.api.controllers;

import com.hawker.api.planters.IndianStatePlanter;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
@RequestMapping("/seed")
public class SeedController {

    private final IndianStatePlanter indianStatePlanter;

    public SeedController(IndianStatePlanter indianStatePlanter) {
        this.indianStatePlanter = indianStatePlanter;
    }

    @RequestMapping(method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<String>> seed() {
        List<String> message = new ArrayList<>();
        message.add(indianStatePlanter.sow());
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

}
