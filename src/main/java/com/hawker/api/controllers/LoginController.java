package com.hawker.api.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.UnAuthorizeException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.resources.UserResource;
import com.hawker.api.services.LoginService;
import com.hawker.api.utils.JsonViews;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class LoginController {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @JsonView(JsonViews.userDetailsExcludingPassword.class)
    @RequestMapping(method = POST, value = "/login")
    public ResponseEntity<ApiResponse> userLogin(@RequestBody UserResource userResource, HttpServletRequest request)
            throws InvalidParameterException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchPaddingException, UnAuthorizeException {
        UserResource loggedInUser = loginService.userLogin(userResource);
        return new ResponseEntity<>(new ApiResponse(loggedInUser, success), HttpStatus.OK);
    }
}
