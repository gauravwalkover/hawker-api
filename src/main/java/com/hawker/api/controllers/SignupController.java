package com.hawker.api.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.exceptions.AlreadyExistException;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.resources.UserResource;
import com.hawker.api.services.SignupService;
import com.hawker.api.utils.JsonViews;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class SignupController {

    private final SignupService signupService;

    public SignupController(SignupService signupService) {
        this.signupService = signupService;
    }

    @JsonView(JsonViews.userDetailsExcludingPassword.class)
    @RequestMapping(method = POST, value = "/signup")
    public ResponseEntity<ApiResponse> userSignup(@RequestBody UserResource userResource, HttpServletRequest request)
            throws InvalidParameterException, AlreadyExistException, InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        UserResource createdUser = signupService.userSignup(userResource);
        return new ResponseEntity<>(new ApiResponse(createdUser, success), HttpStatus.CREATED);
    }

}
