package com.hawker.api.controllers;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.resources.AccountResource;
import com.hawker.api.services.AccountService;
import com.hawker.api.services.BusinessService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
public class AccountController {

    private final BusinessService businessService;
    private final AccountService accountService;

    public AccountController(BusinessService businessService, AccountService accountService) {
        this.businessService = businessService;
        this.accountService = accountService;
    }

    @RequestMapping(method = POST, value = "/business/{businessUniqueName:.+}/accounts")
    public ResponseEntity<ApiResponse> createAccount(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                     @RequestBody AccountResource accountResource,
                                                     HttpServletRequest request)
            throws InvalidParameterException, NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        AccountResource createdAccount = accountService.createAccount(accountResource, business, user);
        return new ResponseEntity<>(new ApiResponse(createdAccount, success), HttpStatus.CREATED);
    }

    @RequestMapping(method = GET, value = "/business/{businessUniqueName:.+}/accounts/{accountUniqueName:.+}")
    public ResponseEntity<ApiResponse> getAccount(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                  @PathVariable(value = "accountUniqueName") String accountUniqueName,
                                                  HttpServletRequest request)
            throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        AccountResource account = accountService.getAccount(accountUniqueName, business);
        return new ResponseEntity<>(new ApiResponse(account, success), HttpStatus.OK);
    }

    @RequestMapping(method = DELETE, value = "/business/{businessUniqueName:.+}/accounts/{accountUniqueName:.+}")
    public ResponseEntity<ApiResponse> deleteItem(@PathVariable(value = "businessUniqueName") String businessUniqueName,
                                                  @PathVariable(value = "accountUniqueName") String accountUniqueName,
                                                  HttpServletRequest request) throws NotFoundException {
        User user = (User) request.getAttribute("principal");
        Business business = businessService.getBusinessByUniqueNameAndCreator(businessUniqueName, user);
        accountService.deleteAccount(accountUniqueName, business);
        return new ResponseEntity<>(new ApiResponse("Account deleted.", success), HttpStatus.OK);
    }

}
