package com.hawker.api.controllers;

import com.hawker.api.models.ApiResponse;
import com.hawker.api.services.BusinessService;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import static com.hawker.api.models.ApiStatus.success;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

@Controller
public class MainController {

    private final BusinessService businessService;

    public MainController(BusinessService businessService) {
        this.businessService = businessService;
    }

    @RequestMapping(method = GET, path = "/")
    public ResponseEntity<ApiResponse> welcome() {
        return new ResponseEntity<>(new ApiResponse("success", success), OK);
    }

    @RequestMapping(method = GET, path = "/ping")
    public ResponseEntity<ApiResponse> ping() {
        return new ResponseEntity<>(new ApiResponse("pong", success), OK);
    }

}
