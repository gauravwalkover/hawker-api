package com.hawker.api.handlers;

import com.hawker.api.exceptions.*;
import com.hawker.api.models.ApiResponse;
import com.hawker.api.utils.MessageUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.beans.PropertyEditorSupport;
import java.util.Set;

import static com.hawker.api.models.ApiStatus.error;

@ControllerAdvice
public class ApiExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(ApiExceptionHandler.class);

    @Autowired
    private MessageUtils messageUtils;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                setValue(text);
            }
        });
    }

    @ExceptionHandler(UnexpectedException.class)
    public ResponseEntity<ApiResponse> unexpectedExceptionHandler(UnexpectedException e) {
        return new ResponseEntity<>(new ApiResponse(messageUtils.t("error.server.unhandled"), error),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(AlreadyExistException.class)
    public ResponseEntity<ApiResponse> alreadyExistExceptionHandler(AlreadyExistException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), error), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(InvalidParameterException.class)
    public ResponseEntity<ApiResponse> invalidParameterExceptionHandler(InvalidParameterException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), error), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InvalidHeaderException.class)
    public ResponseEntity<ApiResponse> invalidHeaderExceptionHandler(InvalidHeaderException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), error), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ApiResponse> notFoundExceptionHandler(NotFoundException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), error), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<ApiResponse> requestMethodNotSupportedExceptionHandler(HttpRequestMethodNotSupportedException e) {
        return new ResponseEntity<>(new ApiResponse(messageUtils.t("error.request.methodNotSupported"), error),
                HttpStatus.METHOD_NOT_ALLOWED);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ApiResponse> httpMessageNotReadableExceptionHandler(HttpMessageNotReadableException e) {
        return new ResponseEntity<>(new ApiResponse(messageUtils.t("error.request.invalidJson"), error),
                HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoHandlerFoundException.class)
    public ResponseEntity<ApiResponse> resourceNotFoundExceptionHandler(NoHandlerFoundException ex, HttpServletRequest req) {
        return new ResponseEntity<>(new ApiResponse(String.format(messageUtils.t("error.request.resourceNotFound"),
                req.getRequestURI()), error), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApiResponse> unhandledExceptionHandler(Exception e) {
        LOGGER.error("Exception: ", e);
        return new ResponseEntity<>(new ApiResponse(messageUtils.t("error.server.unhandled"), error),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ApiResponse> constraintViolationExceptionHandler(ConstraintViolationException e) {
        Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
        String violationMessage = "Invalid parameters provided for this request. ";
        if (constraintViolations.size() > 0) {
            violationMessage = "Following constraint(s) failed: ";
            violationMessage = constraintViolations.stream()
                    .map((constraintViolation) -> constraintViolation.getMessage() + " ")
                    .reduce(violationMessage, String::concat);
        }
        return new ResponseEntity<>(new ApiResponse(violationMessage.trim(), error), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<ApiResponse> invalidRequestExceptionHandler(InvalidRequestException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), error), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(UnAuthorizeException.class)
    public ResponseEntity<ApiResponse> authorizationFailedExceptionHandler(UnAuthorizeException e) {
        return new ResponseEntity<>(new ApiResponse(e.getMessage(), error), HttpStatus.UNAUTHORIZED);
    }

}
