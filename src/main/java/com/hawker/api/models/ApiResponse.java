package com.hawker.api.models;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.utils.JsonViews;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ApiResponse {

    @JsonView(JsonViews.apiResponse.class)
    private Object body;

    @JsonView(JsonViews.apiResponse.class)
    private ApiStatus status;

}
