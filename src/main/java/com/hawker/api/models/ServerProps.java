package com.hawker.api.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ServerProps {

    private String environment;

    private enum Environment {
        LOCAL
    }

    public boolean isLocalEnvironment() {
        return environment.equals(Environment.LOCAL.name());
    }

}
