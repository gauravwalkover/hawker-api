package com.hawker.api.models;

import lombok.Getter;

@Getter
public enum ApiStatus {
    success, error
}
