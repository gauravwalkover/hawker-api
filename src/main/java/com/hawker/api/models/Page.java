package com.hawker.api.models;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.Item;
import com.hawker.api.resources.BusinessResource;
import com.hawker.api.resources.ItemResource;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Page<T> {
    private int page;
    private int count;
    private int totalPages;
    private long totalItems;
    private List<T> items;

    public static <T> Page<T> getPaginatedResults(List<T> data, int page, int count) {
        int totalItems = data.size();
        if (totalItems == 0) {
            return new Page(0, 0, 0, 0, data);
        }
        count = count > 0 ? count : totalItems;
        int totalPages = totalItems / count + (totalItems % count == 0 ? 0 : 1);
        page = page > 0 ? Math.min(page, totalPages) : 1;
        int pageStartAt = Math.min((page - 1) * count, totalItems - 1);
        int pageEndAt = Math.min(pageStartAt + count, totalItems);
        List<T> subList = data.subList(pageStartAt, pageEndAt);
        Page paginatedResult = new Page(page, count, totalPages, totalItems, subList);
        return paginatedResult;
    }

    public static Page<BusinessResource> createBusinessPage(org.springframework.data.domain.Page<Business> businessPage) {
        return new Page<>(businessPage.getNumber() + 1, businessPage.getSize(),
                businessPage.getTotalPages(), businessPage.getTotalElements(),
                businessPage.getContent().stream()
                        .map(b -> new BusinessResource(b))
                        .collect(Collectors.toList()));
    }

    public static Page<ItemResource> createItemPage(org.springframework.data.domain.Page<Item> itemPage) {
        return new Page<>(itemPage.getNumber() + 1, itemPage.getSize(),
                itemPage.getTotalPages(), itemPage.getTotalElements(),
                itemPage.getContent().stream()
                        .map(i -> new ItemResource(i))
                        .collect(Collectors.toList()));
    }
}
