package com.hawker.api.models;

public final class Regex {

    private Regex() {}

    public static final String TAX_NUMBER_FORMAT = "[A-Za-z0-9]{15}";

    public static final String MOBILE_CODE_FORMAT = "[0-9]{2,3}";

    public static final String MOBILE_NUMBER_FORMAT = "[0-9]{8,12}";

    public static final String DD_MM_YYYY_HYPHEN_SEPARATED = "dd-MM-yyyy";

}
