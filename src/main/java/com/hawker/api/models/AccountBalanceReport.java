package com.hawker.api.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hawker.api.dao.models.Entry;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountBalanceReport {

    @JsonIgnore
    private Long id;
    private String name;
    private String uniqueName;
    private String type;
    private String emailId;
    private String mobileCode;
    private String mobileNumber;
    private String addressLine1;
    private String addressLine2;
    private NameCodeModel state;
    private NameCodeModel country;
    private BigDecimal subTotal = BigDecimal.ZERO;
    private BigDecimal taxTotal = BigDecimal.ZERO;
    private BigDecimal grandTotal = BigDecimal.ZERO;

    public AccountBalanceReport(Long id, String name, String uniqueName, String type, String emailId, String mobileCode,
                                String mobileNumber, String addressLine1, String addressLine2, String stateName, String stateCode,
                                String countryName, String countryCode) {
        this.id = id;
        this.name = name;
        this.uniqueName = uniqueName;
        this.type = type;
        this.emailId = emailId;
        this.mobileCode = mobileCode;
        this.mobileNumber = mobileNumber;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.state = new NameCodeModel(stateName, stateCode);
        this.country = new NameCodeModel(countryName, countryCode);
    }

    public void setTotals(Entry accountBalance) {
        this.subTotal = accountBalance.getSubTotal();
        this.taxTotal = accountBalance.getTaxTotal();
        this.grandTotal = accountBalance.getGrandTotal();
    }
}
