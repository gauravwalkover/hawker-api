package com.hawker.api.models;

import lombok.Getter;

@Getter
public enum PaymentStatus {
    PAID, UNPAID, CANCELLED, RETURNED
}
