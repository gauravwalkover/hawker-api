package com.hawker.api.models;

import lombok.Getter;

@Getter
public enum AccountType {
    CUSTOMER, VENDOR, BANK, CASH
}
