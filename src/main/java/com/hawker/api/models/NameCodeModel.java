package com.hawker.api.models;

import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.models.State;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NameCodeModel {

    private String name;
    private String code;

}
