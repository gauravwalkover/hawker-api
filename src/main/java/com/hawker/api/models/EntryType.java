package com.hawker.api.models;

import lombok.Getter;

@Getter
public enum EntryType {
    SALE, PURCHASE
}
