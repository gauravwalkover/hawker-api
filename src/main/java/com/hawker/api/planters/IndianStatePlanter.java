package com.hawker.api.planters;

import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.models.State;
import com.hawker.api.dao.repositories.CountryRepository;
import com.hawker.api.dao.repositories.StateRepository;
import com.hawker.api.utils.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.common.base.Function;

import static java.util.Arrays.asList;
import static com.google.common.collect.Lists.transform;
import com.google.common.base.Joiner;

@Component
public class IndianStatePlanter {

    @Autowired
    private StateRepository stateRepository;

    @Autowired
    private CountryRepository countryRepository;



    public String sow() {
        try {
            State[] states = JsonUtils.readJson("/db/seed/indian_states.json", State[].class);
            Optional<Country> optionalCountry = countryRepository.findByCode(Country.INDIA);
            if(optionalCountry.isPresent()) {
                Country country = optionalCountry.get();
                Stream.of(states).forEach(s -> s.setCountry(country));
            }
            return createIfNeeded(states);
        } catch (Exception e) {
            return String.format("An error occurred while seeding the states. Please inspect the server logs. %s", e);
        }
    }

    private String createIfNeeded(State[] states) {
        List<String> stateNames = transform(asList(states), toNames());
        return String.format("Created following states : %s", Joiner.on(",").join(stateNames));
    }

    private Function<State, String> toNames() {
        return (State incoming) -> {
            Optional<State> optionalState = stateRepository.findByCodeAndCountryCode(incoming.getCode(), incoming.getCountry().getCode());
            if (!optionalState.isPresent()) {
                stateRepository.save(incoming);
            }
            return incoming.getName();
        };
    }
}
