package com.hawker.api.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;

@Component
public class JsonUtils {

    public static <T> T parseJson(String json, Class<T> clazz) throws IOException {
        if (StringUtils.isBlank(json)) {
            return null;
        }
        return new CustomObjectMapper().readValue(json, clazz);
    }

    public static String toJson(Object arg) throws JsonProcessingException {
        return new CustomObjectMapper().writeValueAsString(arg);
    }

    public static String toJsonAsString(Object arg) throws JsonProcessingException {
        if (arg == null) {
            return null;
        }
        return new CustomObjectMapper()
                .setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY)
                .writeValueAsString(arg).replaceAll("\\\\", "");
    }

    public static <T> T readJson(String filePath, Class<T> clazz) throws IOException {
        return new CustomObjectMapper().readValue(fixture(filePath), clazz);
    }

    private static InputStream fixture(String fixture) {
        return JsonUtils.class.getResourceAsStream(fixture);
    }

}
