package com.hawker.api.utils;

import com.google.common.collect.Maps;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.models.Regex;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class DateUtils {

    private static final Logger LOGGER = LogManager.getLogger(DateUtils.class);

    private static MessageUtils messageUtils;

    @Autowired
    public DateUtils(MessageUtils messageUtils) {
        DateUtils.messageUtils = messageUtils;
    }

    public static LocalDate parseString(String dateString) throws InvalidParameterException {
        if (StringUtils.isBlank(dateString)) {
            LOGGER.warn("Got null or empty date string in parse String function.");
            return new LocalDate();
        }
        try {
            return LocalDate.parse(dateString, DateTimeFormat.forPattern(Regex.DD_MM_YYYY_HYPHEN_SEPARATED));
        } catch (Exception e) {
            LOGGER.debug("parseString() : " + dateString);
            throw new InvalidParameterException(messageUtils.t("error.general.invalidDate"));
        }
    }

    @Getter
    @AllArgsConstructor
    public enum HawkerDate {
        FROMDATE, TODATE;
    }

    public static Map<HawkerDate, LocalDate> getDatePeriod(String from, String to) throws InvalidParameterException {
        LocalDate fromDate = StringUtils.isBlank(from) ? new LocalDate().minusMonths(1) : parseString(from);
        LocalDate toDate = StringUtils.isBlank(to) ? new LocalDate() : parseString(to);
        toDate = fromDate.isAfter(toDate) ? fromDate : toDate; //handling condition if ever fromDate is after toDate
        Map<HawkerDate, LocalDate> datePeriodMap = Maps.newHashMap();
        datePeriodMap.put(HawkerDate.FROMDATE, fromDate);
        datePeriodMap.put(HawkerDate.TODATE, toDate);
        return datePeriodMap;
    }

}
