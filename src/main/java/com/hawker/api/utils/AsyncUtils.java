package com.hawker.api.utils;

import com.hawker.api.exceptions.UnexpectedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@Component
public class AsyncUtils {

    private static final Logger LOGGER = LogManager.getLogger(AsyncUtils.class);

    private static MessageUtils messageUtils;

    @Autowired
    public AsyncUtils(MessageUtils messageUtils) {
        AsyncUtils.messageUtils = messageUtils;
    }

    public static <T> T getValueOfFutureObject(Future<T> futureObj) throws UnexpectedException {
        try {
            return futureObj.get();
        } catch (ExecutionException ex) {
            throw new UnexpectedException(messageUtils.t("error.server.unhandled"));
        } catch (InterruptedException ex) {
            throw new UnexpectedException(messageUtils.t("error.server.unhandled"));
        }
    }

}
