package com.hawker.api.utils;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.UnAuthorizeException;
import com.hawker.api.models.Regex;
import com.hawker.api.models.ServerProps;
import com.hawker.api.resources.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ValidationUtils {

    private static final Logger LOGGER = LogManager.getLogger(ValidationUtils.class);

    private static MessageUtils messageUtils;
    private static ServerProps serverProps;

    @Autowired
    public ValidationUtils(MessageUtils messageUtils, ServerProps serverProps) {
        ValidationUtils.messageUtils = messageUtils;
        ValidationUtils.serverProps = serverProps;
    }

    private static EmailValidator emailValidator = EmailValidator.getInstance();
    private static PhoneNumberUtil mobileValidator = PhoneNumberUtil.getInstance();

    public static void validateUserSignupRequest(UserResource userResource) throws InvalidParameterException {
        if (!isValidEmail(userResource.getEmailId())) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidEmail"));
        }
        if (StringUtils.isBlank(userResource.getName())) {
            throw new InvalidParameterException(messageUtils.t("error.user.invalidName"));
        }
        validatePassword(userResource.getPassword());
    }

    public static void validateUserLoginRequest(UserResource userResource) throws InvalidParameterException {
        if (!isValidEmail(userResource.getEmailId())) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidEmail"));
        }
        if (StringUtils.isBlank(userResource.getPassword())) {
            throw new InvalidParameterException(messageUtils.t("error.user.invalidPassword"));
        }
    }

    public static boolean isValidEmail(String email) {
        if(StringUtils.isBlank(email)) {
            return false;
        }
        return emailValidator.isValid(email);
    }

    public static boolean isValidMobileNumber(String mobileCode, String mobileNumber) {
        if(StringUtils.isAnyBlank(mobileCode, mobileNumber)) {
            return false;
        }
        if(!mobileCode.matches(Regex.MOBILE_CODE_FORMAT)) {
            return false;
        }
        if(!mobileNumber.matches(Regex.MOBILE_NUMBER_FORMAT)) {
            return false;
        }
        Phonenumber.PhoneNumber phoneNumber = new Phonenumber.PhoneNumber()
                .setCountryCode(Integer.parseInt(mobileCode))
                .setNationalNumber(Long.parseLong(mobileNumber));
        return mobileValidator.isValidNumber(phoneNumber);
    }

    public static String validatePassword(String password) throws InvalidParameterException {
        if (StringUtils.isBlank(password)) {
            throw new InvalidParameterException(messageUtils.t("error.user.invalidPassword"));
        }
        /**
         * Minimum eight characters, maximum 20 characters, at least one uppercase
         * letter, one lowercase letter, one number and one special character.
         */
        String regex = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&_])[A-Za-z\\d$@$!%*?&_]{8,20}";

        if (!password.matches(regex)) {
            throw new InvalidParameterException(messageUtils.t("error.user.invalidPassword"));
        }
        return password;
    }

    public static void validateEmailIdBelongsToUser(String emailId, User principal) throws InvalidParameterException, UnAuthorizeException {
        if(!isValidEmail(emailId)) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidEmail"));
        }
        if(!principal.getEmailId().equals(emailId)) {
            throw new UnAuthorizeException(messageUtils.t("error.auth.unauthorize"));
        }
    }

    public static void validateCreateBusinessRequest(BusinessResource businessResource) throws InvalidParameterException {
        if (!isValidEmail(businessResource.getEmailId())) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidEmail"));
        }
        if (StringUtils.isBlank(businessResource.getName())) {
            throw new InvalidParameterException(messageUtils.t("error.business.invalidName"));
        }
        if(businessResource.getCountry() == null || StringUtils.isBlank(businessResource.getCountry().getCode())) {
            throw new InvalidParameterException(messageUtils.t("error.business.countryMandatory"));
        }
        if(businessResource.getState() == null || StringUtils.isBlank(businessResource.getState().getCode())) {
            throw new InvalidParameterException(messageUtils.t("error.business.stateMandatory"));
        }
        if(StringUtils.isBlank(businessResource.getAddressLine1())) {
            throw new InvalidParameterException(messageUtils.t("error.business.addressLine1Mandatory"));
        }
        if(StringUtils.isBlank(businessResource.getAddressLine2())) {
            throw new InvalidParameterException(messageUtils.t("error.business.addressLine2Mandatory"));
        }
        if(StringUtils.isNotBlank(businessResource.getTaxNumber()) && !businessResource.getTaxNumber().matches(Regex.TAX_NUMBER_FORMAT)) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidTaxNumber"));
        }
        if(StringUtils.isBlank(businessResource.getMobileCode())) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidMobileCode"));
        }
        if(StringUtils.isBlank(businessResource.getMobileNumber())) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidMobileNumber"));
        }
        String mobileCode = businessResource.getMobileCode();
        String mobileNumber = businessResource.getMobileNumber();
        if(!isValidMobileNumber(mobileCode, mobileNumber)) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidMobileCodeOrNumber"));
        }
    }

    public static void validateCreateItemRequest(ItemResource itemResource) throws InvalidParameterException {
        if (StringUtils.isBlank(itemResource.getName())) {
            throw new InvalidParameterException(messageUtils.t("error.item.invalidName"));
        }
        if (itemResource.getItemCategory() == null) {
            throw new InvalidParameterException(messageUtils.t("error.item.invalidItemCategory"));
        }
        if (CollectionUtils.isEmpty(itemResource.getItemPrices())) {
            throw new InvalidParameterException(messageUtils.t("error.item.invalidItemPrices"));
        }
    }

    public static void validateCreateAccountRequest(AccountResource accountResource) throws InvalidParameterException {
        if (StringUtils.isNotBlank(accountResource.getEmailId()) && !isValidEmail(accountResource.getEmailId())) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidEmail"));
        }
        if (StringUtils.isBlank(accountResource.getName())) {
            throw new InvalidParameterException(messageUtils.t("error.account.invalidName"));
        }
        if(accountResource.getCountry() != null && StringUtils.isBlank(accountResource.getCountry().getCode())) {
            throw new InvalidParameterException(messageUtils.t("error.account.countryMandatory"));
        }
        if(accountResource.getState() != null && StringUtils.isBlank(accountResource.getState().getCode())) {
            throw new InvalidParameterException(messageUtils.t("error.account.stateMandatory"));
        }
        if(StringUtils.isBlank(accountResource.getType())) {
            throw new InvalidParameterException(messageUtils.t("error.account.invalidType"));
        }
        String mobileCode = accountResource.getMobileCode();
        String mobileNumber = accountResource.getMobileNumber();
        if(StringUtils.isNoneBlank(mobileCode, mobileNumber) && !isValidMobileNumber(mobileCode, mobileNumber)) {
            throw new InvalidParameterException(messageUtils.t("error.general.invalidMobileCodeOrNumber"));
        }
    }

    public static void validateCreateEntryRequest(EntryResource entryResource) throws InvalidParameterException {
        if (StringUtils.isBlank(entryResource.getType())) {
            throw new InvalidParameterException(messageUtils.t("error.entry.invalidType"));
        }
        if (StringUtils.isBlank(entryResource.getPaymentStatus())) {
            throw new InvalidParameterException(messageUtils.t("error.entry.invalidPaymentStatus"));
        }
        validateCreateTransactionsRequest(entryResource.getTransactions());
    }

    public static void validateCreateTransactionsRequest(List<TransactionResource> transactionResources) throws InvalidParameterException {
        if(CollectionUtils.isEmpty(transactionResources)) {
            throw new InvalidParameterException(messageUtils.t("error.transactions.empty"));
        }
        /**
         * Validations can be added for checking unique items in the list and also adding a max limit for the number
         * of transactions that can be received in a single request.
         */
        for(TransactionResource transactionResource : transactionResources) {
            if(transactionResource.getItem() == null || StringUtils.isBlank(transactionResource.getItem().getUniqueName())) {
                throw new InvalidParameterException(messageUtils.t("error.transactions.invalidItem"));
            }
            if(transactionResource.getItemUnit() == null || StringUtils.isBlank(transactionResource.getItemUnit().getCode())) {
                throw new InvalidParameterException(messageUtils.t("error.transactions.invalidItemUnit"));
            }
            if(transactionResource.getAmount() == null) {
                throw new InvalidParameterException(messageUtils.t("error.transactions.invalidAmount"));
            }
            if(transactionResource.getQuantity() == null) {
                throw new InvalidParameterException(messageUtils.t("error.transactions.invalidQuantity"));
            }
        }
    }

}
