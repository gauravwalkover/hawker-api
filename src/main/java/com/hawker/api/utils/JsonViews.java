package com.hawker.api.utils;

import org.springframework.stereotype.Component;

@Component
public class JsonViews {

    public interface apiResponse {}

    public interface userAuthKey {}

    public interface userEmailId {}

    public interface userName {}

    public interface userPassword {}

    public interface userDetailsExcludingPassword extends apiResponse, userAuthKey, userEmailId, userName {}

    public interface itemName {}

    public interface itemUniqueName {}

    public interface itemDescription {}

    public interface itemCategoryCode {}

    public interface itemCategoryName {}

    public interface itemCategory extends itemCategoryCode, itemCategoryName {}

    public interface itemPrices {}

    public interface itemExcludingItemPrices extends apiResponse, itemName, itemUniqueName, itemDescription, itemCategory {}

}
