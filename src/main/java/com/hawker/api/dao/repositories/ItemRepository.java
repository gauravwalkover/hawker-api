package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.Item;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {

    Item findByUniqueNameAndBusiness(String uniqueName, Business business);

    @Query("SELECT it FROM Item it JOIN FETCH it.itemPrices ip WHERE it.uniqueName = :uniqueName AND it.business = :business")
    Item getItemJoinedByItemPricesByUniqueNameAndBusiness(@Param("uniqueName") String uniqueName,
                                                          @Param("business") Business business);

    @Query(value = "SELECT it FROM Item it JOIN FETCH it.itemPrices ip WHERE it.business = :business",
            countQuery = "SELECT COUNT(it) FROM Item it JOIN it.itemPrices ip WHERE it.business = :business")
    Page<Item> getItemPageJoinedByItemPricesByBusiness(@Param("business") Business business, Pageable pageable);
}
