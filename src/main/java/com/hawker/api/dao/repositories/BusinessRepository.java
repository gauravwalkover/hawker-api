package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessRepository extends CrudRepository<Business, Long>, PagingAndSortingRepository<Business, Long> {

    Business findByUniqueNameAndCreatedBy(String uniqueName, User user);

    Page<Business> findByCreatedBy(User user, Pageable pageable);

}
