package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findByEmailId(String emailId);

    Optional<User> findByEmailIdAndPassword(String emailId, String password);

    @Query(value = "select case when count(u.id) > 0 then true else false end as email_id_exists from users u where u.email_id = :emailId",
            nativeQuery = true)
    boolean doesEmailIdExist(@Param("emailId") String emailId);
}
