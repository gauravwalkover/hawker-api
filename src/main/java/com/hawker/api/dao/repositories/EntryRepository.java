package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.Entry;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface EntryRepository extends CrudRepository<Entry, Long> {

    @Query("SELECT e FROM Entry e JOIN FETCH e.transactions t WHERE e.uniqueName = :uniqueName AND e.business = :business")
    Entry getEntryJoinedByTransactionsByUniqueNameAndBusiness(@Param("uniqueName") String uniqueName,
                                                              @Param("business") Business business);

    @Query(nativeQuery = true)
    List<Entry> getAccountBalancesByBusinessAndPaymentStatusAndBetweenDates(@Param("businessId") Long businessId,
                                                                            @Param("paymentStatus") String paymentStatus,
                                                                            @Param("fromDate") Date fromDate,
                                                                            @Param("toDate") Date toDate);
}
