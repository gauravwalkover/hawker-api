package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.ItemCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemCategoryRepository extends CrudRepository<ItemCategory, Long> {

    ItemCategory findByCode(String code);

}
