package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.ItemUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemUnitRepository extends CrudRepository<ItemUnit, Long> {
}
