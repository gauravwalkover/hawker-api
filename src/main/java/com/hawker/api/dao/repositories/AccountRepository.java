package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.Account;
import com.hawker.api.dao.models.Business;
import com.hawker.api.models.AccountBalanceReport;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {

    Account getAccountByUniqueNameAndBusiness(String uniqueName, Business business);

    @Query(nativeQuery = true)
    List<AccountBalanceReport> getAccountDetailsForAccountBalanceReportByBusiness(@Param("businessId") Long businessId);
}
