package com.hawker.api.dao.repositories;

import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.models.State;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StateRepository extends CrudRepository<State, Long> {

    Optional<State> findByCodeAndCountry(String code, Country country);

    @Query(value = "select s.* from state s join country c on s.country_id = c.id " +
            "where s.code = :code and c.code = :countryCode", nativeQuery = true)
    Optional<State> findByCodeAndCountryCode(@Param("code") String code, @Param("countryCode") String countryCode);
}
