package com.hawker.api.dao.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "business")
public class Business extends BaseModel {

    @NotNull(message = "Business name cannot be blank.")
    @Column(name = "name")
    @Size(min = 1, max = 50, message = "Name is mandatory. It should be 1-50 characters long.")
    private String name;

    @NotNull(message = "Business unique name cannot be blank.")
    @Column(name = "unique_name")
    private String uniqueName;

    @NotNull(message = "Email id cannot be blank.")
    @Column(name = "email_id")
    private String emailId;

    @NotNull(message = "Address line 1 cannot be blank.")
    @Column(name = "address_line_1")
    @Size(min = 1, max = 50, message = "Address line 1 is mandatory. It should be 1-50 characters long.")
    private String addressLine1;

    @NotNull(message = "Address line 2 cannot be blank.")
    @Column(name = "address_line_2")
    @Size(min = 1, max = 50, message = "Address line 2 is mandatory. It should be 1-50 characters long.")
    private String addressLine2;

    @JoinColumn(name = "state_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private State state;

    @JoinColumn(name = "country_id")
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private Country country;

    @NotNull(message = "Mobile code cannot be blank.")
    @Column(name = "mobile_code")
    @Pattern(regexp = "[0-9]{2,3}", message = "Mobile code format is invalid.")
    private String mobileCode;

    @NotNull(message = "Mobile number cannot be blank.")
    @Column(name = "mobile_number")
    @Pattern(regexp = "[0-9]{8,12}", message = "Mobile number format is invalid.")
    private String mobileNumber;

    @Column(name = "tax_number")
    private String taxNumber;

    public void generateUniqueName() {
        this.uniqueName = RandomStringUtils.randomAlphanumeric(6);
    }

}
