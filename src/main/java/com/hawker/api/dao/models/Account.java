package com.hawker.api.dao.models;

import com.hawker.api.models.AccountBalanceReport;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Account.getAccountDetailsForAccountBalanceReportByBusiness",
                query = "SELECT a.id, a.name, a.unique_name, a.type, a.email_id, a.mobile_code, a.mobile_number, " +
                        "a.address_line_1, a.address_line_2, " +
                        "s.name AS state_name, s.code AS state_code, c.name AS country_name, c.code AS country_code " +
                        "FROM account a " +
                        "LEFT JOIN state s on s.id = a.state_id " +
                        "LEFT JOIN country c on c.id = a.country_id " +
                        "WHERE a.business_id = :businessId",
                resultSetMapping = "accountBalanceReport"
        )
})
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "accountBalanceReport", classes = {
                @ConstructorResult(
                        targetClass = AccountBalanceReport.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "unique_name", type = String.class),
                                @ColumnResult(name = "type", type = String.class),
                                @ColumnResult(name = "email_id", type = String.class),
                                @ColumnResult(name = "mobile_code", type = String.class),
                                @ColumnResult(name = "mobile_number", type = String.class),
                                @ColumnResult(name = "address_line_1", type = String.class),
                                @ColumnResult(name = "address_line_2", type = String.class),
                                @ColumnResult(name = "state_name", type = String.class),
                                @ColumnResult(name = "state_code", type = String.class),
                                @ColumnResult(name = "country_name", type = String.class),
                                @ColumnResult(name = "country_code", type = String.class)
                        }
                )
        })
})


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account")
public class Account extends BaseModel {

    @NotNull(message = "Account name cannot be blank.")
    @Column(name = "name")
    @Size(min = 1, max = 100, message = "Name is mandatory. It should be 1-100 characters long.")
    private String name;

    @NotNull(message = "Account unique name cannot be blank.")
    @Column(name = "unique_name")
    private String uniqueName;

    @NotNull(message = "Account business cannot be null.")
    @JoinColumn(nullable = false, name = "business_id")
    @ManyToOne(optional = false)
    private Business business;

    @NotNull(message = "Account type cannot be blank.")
    @Column(name = "type")
    @Pattern(regexp = "CUSTOMER|VENDOR|CASH|BANK", message = "Account type is invalid.")
    private String type;

    @Column(name = "is_active")
    private boolean isActive = true;

    @Column(name = "email_id")
    private String emailId;

    @Column(name = "address_line_1")
    @Size(min = 0, max = 50, message = "Address line 1 is mandatory. It should be 1-50 characters long.")
    private String addressLine1;

    @Column(name = "address_line_2")
    @Size(min = 0, max = 50, message = "Address line 2 is mandatory. It should be 1-50 characters long.")
    private String addressLine2;

    @JoinColumn(name = "state_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private State state;

    @JoinColumn(name = "country_id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Country country;

    @Column(name = "mobile_code")
    @Pattern(regexp = "^$|[0-9]{2,3}", message = "Mobile code format is invalid.")
    private String mobileCode;

    @Column(name = "mobile_number")
    @Pattern(regexp = "^$|[0-9]{8,12}", message = "Mobile number format is invalid.")
    private String mobileNumber;

    public Account(Long accountId) {
        super(accountId);
    }

    public void generateUniqueName() {
        this.uniqueName = RandomStringUtils.randomAlphanumeric(6);
    }

}
