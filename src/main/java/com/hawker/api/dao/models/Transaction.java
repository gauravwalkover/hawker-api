package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "transaction")
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "entry_id")
    @ManyToOne(optional = false)
    private Entry entry;

    @JoinColumn(name = "item_id")
    @ManyToOne(optional = false)
    private Item item;

    @JoinColumn(name = "item_unit_id")
    @ManyToOne(optional = false)
    private ItemUnit itemUnit;

    @Column(name = "quantity")
    @Min(value = 0, message = "Quantity must be a positive value.")
    @Max(value = 9999999L, message = "Quantity must be less than or equal to 9999999.")
    private BigDecimal quantity;

    @Column(name = "amount")
    @Min(value = 0, message = "Amount must be a positive value.")
    @Max(value = 9999999L, message = "Amount must be less than or equal to 9999999.")
    private BigDecimal amount;

}
