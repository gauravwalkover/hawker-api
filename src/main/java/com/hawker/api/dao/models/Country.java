package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "country")
public class Country {

    public final static String INDIA = "IN";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @NotNull(message = "Country name cannot be blank.")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Country code cannot be blank.")
    @Column(name = "code")
    private String code;

    public Country(String code) {
        this.code = code;
    }

}
