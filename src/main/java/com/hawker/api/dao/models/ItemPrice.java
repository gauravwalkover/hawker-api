package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "item_price")
public class ItemPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @JoinColumn(name = "item_id")
    @ManyToOne(optional = false)
    private Item item;

    @JoinColumn(name = "item_unit_id")
    @ManyToOne(optional = false)
    private ItemUnit itemUnit;

    @Column(name = "price")
    @Min(value = 0, message = "Price must be a positive value.")
    @Max(value = 9999999L, message = "Price must be less than or equal to 9999999.")
    private BigDecimal price;

}
