package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "state")
public class State {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @NotNull(message = "State name cannot be blank.")
    @Column(name = "name")
    private String name;

    @NotNull(message = "State code cannot be blank.")
    @Column(name = "code")
    private String code;

    @NotNull
    @JoinColumn(name = "country_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected Country country;

    public State(String code) {
        this.code = code;
    }

}
