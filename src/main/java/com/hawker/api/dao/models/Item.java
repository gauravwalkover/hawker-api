package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.BatchSize;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "item")
public class Item extends BaseModel {

    @NotNull(message = "Item name cannot be blank.")
    @Column(name = "name")
    @Size(min = 1, max = 100, message = "Name is mandatory. It should be 1-100 characters long.")
    private String name;

    @NotNull(message = "Item unique name cannot be blank.")
    @Column(name = "unique_name")
    private String uniqueName;

    @NotNull(message = "Item category cannot be null.")
    @JoinColumn(nullable = false, name = "item_category_id")
    @ManyToOne(optional = false)
    private ItemCategory itemCategory;

    @Column(name = "description")
    @Size(min = 0, max = 250, message = "Description can be 250 characters long.")
    private String description;

    @NotNull(message = "Item business cannot be null.")
    @JoinColumn(nullable = false, name = "business_id")
    @ManyToOne(optional = false)
    private Business business;

    @JsonIgnore
    @OneToMany(mappedBy = "item", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    public List<ItemPrice> itemPrices = new ArrayList<>(0);

    public void generateUniqueName() {
        this.uniqueName = RandomStringUtils.randomAlphanumeric(6);
    }

    public void addAllItemPrices(List<ItemPrice> itemPrices) {
        this.itemPrices.addAll(itemPrices);
    }
}
