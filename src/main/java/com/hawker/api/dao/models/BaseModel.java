package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public class BaseModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @JsonIgnore
    @JoinColumn(name = "created_by")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected User createdBy;

    @JsonIgnore
    @JoinColumn(name = "updated_by")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    protected User updatedBy;

    @JsonIgnore
    @Type(type = "localDateTime")
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @JsonIgnore
    @Type(type = "localDateTime")
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @PreUpdate
    public void onUpdate() {
        updatedAt = new LocalDateTime();
    }

    @PrePersist
    public void onCreate() {
        updatedAt = createdAt = new LocalDateTime();
        createdBy = updatedBy;
    }

    protected BaseModel(Long id) {
        this.id = id;
    }

}
