package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDate;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@NamedNativeQueries({
        @NamedNativeQuery(
                name = "Entry.getAccountBalancesByBusinessAndPaymentStatusAndBetweenDates",
                query = "SELECT e.account_id AS account_id, COALESCE(SUM(e.sub_total), 0) AS sub_total, COALESCE(SUM(e.tax_total), 0) AS tax_total, " +
                        "COALESCE(SUM(e.grand_total), 0) AS grand_total " +
                        "FROM entry e " +
                        "WHERE e.entry_date BETWEEN :fromDate AND :toDate AND e.business_id = :businessId AND e.payment_status = :paymentStatus " +
                        "GROUP BY e.account_id",
                resultSetMapping = "accountBalances"
        )
})
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "accountBalances", classes = {
                @ConstructorResult(
                        targetClass = Entry.class,
                        columns = {
                                @ColumnResult(name = "account_id", type = Long.class),
                                @ColumnResult(name = "sub_total", type = BigDecimal.class),
                                @ColumnResult(name = "tax_total", type = BigDecimal.class),
                                @ColumnResult(name = "grand_total", type = BigDecimal.class)
                        }
                )
        })
})


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "entry")
public class Entry extends BaseModel {

    @NotNull(message = "Entry unique name cannot be blank.")
    @Column(name = "unique_name")
    private String uniqueName;

    @Column(name = "entry_date")
    @Type(type = "localDate")
    @NotNull(message = "Entry date cannot be null.")
    private LocalDate entryDate;

    @Column(name = "description")
    @Size(min = 0, max = 250, message = "Description can be 250 characters long.")
    private String description;

    @NotNull(message = "Entry account cannot be null.")
    @JoinColumn(nullable = false, name = "account_id")
    @ManyToOne(optional = false)
    private Account account;

    @NotNull(message = "Entry business cannot be null.")
    @JoinColumn(nullable = false, name = "business_id")
    @ManyToOne(optional = false)
    private Business business;

    @NotNull(message = "Entry type cannot be blank.")
    @Column(name = "type")
    @Pattern(regexp = "SALE|PURCHASE", message = "Entry type is invalid.")
    private String type;

    @NotNull(message = "Entry payment status cannot be blank.")
    @Column(name = "payment_status")
    @Pattern(regexp = "PAID|UNPAID|CANCELLED|RETURNED", message = "Entry payment status is invalid.")
    private String paymentStatus;

    @Column(name = "sub_total")
    @Min(value = 0, message = "Sub total must be a positive value.")
    @Max(value = 9999999L, message = "Sub total must be less than or equal to 9999999.")
    private BigDecimal subTotal;

    @Column(name = "tax_total")
    @Min(value = 0, message = "Tax total must be a positive value.")
    @Max(value = 9999999L, message = "Tax total must be less than or equal to 9999999.")
    private BigDecimal taxTotal;

    @Column(name = "grand_total")
    @Min(value = 0, message = "Grand total must be a positive value.")
    @Max(value = 9999999999L, message = "Grand total must be less than or equal to 9999999999.")
    private BigDecimal grandTotal;

    @JsonIgnore
    @OneToMany(mappedBy = "entry", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    public List<Transaction> transactions = new ArrayList<>(0);

    public void generateUniqueName() {
        this.uniqueName = RandomStringUtils.randomAlphanumeric(6);
    }

    public void calculateTotals() {
        this.subTotal = transactions.stream()
                .map(t -> t.getAmount())
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        this.taxTotal = BigDecimal.ZERO;
        this.grandTotal = BigDecimal.ZERO
                .add(this.subTotal)
                .add(this.taxTotal);
    }

    public Entry(Long accountId, BigDecimal subTotal, BigDecimal taxTotal, BigDecimal grandTotal) {
        this.account = new Account(accountId);
        this.subTotal = subTotal;
        this.taxTotal = taxTotal;
        this.grandTotal = grandTotal;
    }

}
