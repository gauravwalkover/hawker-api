package com.hawker.api.dao.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hawker.api.utils.SecurityUtils;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    @Column(name = "id")
    private Long id;

    @NotNull(message = "User name cannot be blank.")
    @Size(min = 1, max = 50, message = "Name is mandatory. It should be 1-50 characters long.")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Email id cannot be blank.")
    @Column(name = "email_id")
    private String emailId;

    @NotNull(message = "Password cannot be blank.")
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Column(name = "hash_key")
    private String hashKey;

    @JsonIgnore
    @Type(type = "localDateTime")
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @JsonIgnore
    @Type(type = "localDateTime")
    @Column(name = "updated_at")
    private LocalDateTime updatedAt;

    @PreUpdate
    public void onUpdate() {
        updatedAt = new LocalDateTime();
    }

    @PrePersist
    public void onCreate() {
        updatedAt = createdAt = new LocalDateTime();
    }

    public User(String emailId, String password) {
        this.emailId = emailId;
        this.password = password;
    }

    public void createHashKey() {
        this.hashKey = RandomStringUtils.randomAlphanumeric(10);
    }

    @Transient
    private String authKey;

    public void generateAuthKey() throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchPaddingException {
        this.authKey = SecurityUtils.generateAuthKeyForUser(this);
    }
}
