package com.hawker.api.exceptions;

public class UnexpectedException extends Exception {

    public UnexpectedException(String message) {
        super(message);
    }

}
