package com.hawker.api.exceptions;

public class InvalidHeaderException extends Exception {

    public InvalidHeaderException(String message) {
        super(message);
    }

}
