package com.hawker.api.exceptions;

public class InvalidParameterException extends Exception {

    public InvalidParameterException(String message) {
        super(message);
    }

}
