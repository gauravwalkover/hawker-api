package com.hawker.api.exceptions;

public class UnAuthorizeException extends Exception {

    public UnAuthorizeException(String message) {
        super(message);
    }

}
