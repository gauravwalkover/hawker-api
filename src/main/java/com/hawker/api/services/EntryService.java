package com.hawker.api.services;

import com.hawker.api.dao.models.Account;
import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.Entry;
import com.hawker.api.dao.models.User;
import com.hawker.api.dao.repositories.EntryRepository;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.resources.EntryResource;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.ValidationUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EntryService {

    private final EntryRepository entryRepository;
    private final TransactionService transactionService;
    private final MessageUtils messageUtils;

    public EntryService(EntryRepository entryRepository, TransactionService transactionService, MessageUtils messageUtils) {
        this.entryRepository = entryRepository;
        this.transactionService = transactionService;
        this.messageUtils = messageUtils;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public EntryResource createEntry(EntryResource entryResource, Account account, Business business, User user)
            throws InvalidParameterException, NotFoundException {
        ValidationUtils.validateCreateEntryRequest(entryResource);
        Entry entry = entryResource.getModel();
        transactionService.validateTransactionsForEntry(entry, business);
        entry.calculateTotals();
        entry.setAccount(account);
        entry.setBusiness(business);
        entry.setUpdatedBy(user);
        entry.generateUniqueName();
        entry = entryRepository.save(entry);
        return new EntryResource(entry);
    }

    @Transactional(readOnly = true)
    public EntryResource getEntry(String entryUniqueName, Business business) throws NotFoundException {
        Entry entry = getEntryJoinedByTransactionsByUniqueNameAndBusiness(entryUniqueName, business);
        return new EntryResource(entry);
    }

    public Entry getEntryJoinedByTransactionsByUniqueNameAndBusiness(String uniqueName, Business business) throws NotFoundException {
        Entry entry = entryRepository.getEntryJoinedByTransactionsByUniqueNameAndBusiness(uniqueName, business);
        if (entry == null) {
            throw new NotFoundException(messageUtils.t("error.entry.notFound"));
        }
        return entry;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public void deleteEntry(String entryUniqueName, Business business) throws NotFoundException {
        Entry entry = getEntryJoinedByTransactionsByUniqueNameAndBusiness(entryUniqueName, business);
        entryRepository.delete(entry);
    }

    @Transactional(rollbackFor = {Throwable.class})
    public EntryResource updateEntryPatch(Entry existingEntry, EntryResource entryResource, User user) {
        if(StringUtils.isNotBlank(entryResource.getPaymentStatus())) {
            existingEntry.setPaymentStatus(entryResource.getPaymentStatus());
        }
        existingEntry.setUpdatedBy(user);
        existingEntry = entryRepository.save(existingEntry);
        return new EntryResource(existingEntry);
    }
}
