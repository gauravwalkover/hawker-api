package com.hawker.api.services;

import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.models.State;
import com.hawker.api.dao.repositories.StateRepository;
import com.hawker.api.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StateService {

    private final StateRepository stateRepository;

    public StateService(StateRepository stateRepository) {
        this.stateRepository = stateRepository;
    }

    public State getStateByCodeAndCountry(String code, Country country) throws NotFoundException {
        Optional<State> optionalState = stateRepository.findByCodeAndCountry(code, country);
        return optionalState.orElseThrow(() -> new NotFoundException("error.state.notFound"));
    }

    public State getStateByCodeAndCountryCode(String code, String countryCode) throws NotFoundException {
        Optional<State> optionalState = stateRepository.findByCodeAndCountryCode(code, countryCode);
        return optionalState.orElseThrow(() -> new NotFoundException("error.state.notFound"));
    }
}
