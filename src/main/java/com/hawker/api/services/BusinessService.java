package com.hawker.api.services;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.models.State;
import com.hawker.api.dao.models.User;
import com.hawker.api.dao.repositories.BusinessRepository;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.Page;
import com.hawker.api.resources.BusinessResource;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.ValidationUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BusinessService {

    private final BusinessRepository businessRepository;
    private final CountryService countryService;
    private final StateService stateService;
    private final MessageUtils messageUtils;

    public BusinessService(BusinessRepository businessRepository, CountryService countryService, StateService stateService,
                           MessageUtils messageUtils) {
        this.businessRepository = businessRepository;
        this.countryService = countryService;
        this.stateService = stateService;
        this.messageUtils = messageUtils;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public BusinessResource createBusiness(BusinessResource businessResource, User user) throws InvalidParameterException, NotFoundException {
        ValidationUtils.validateCreateBusinessRequest(businessResource);
        Business business = businessResource.getModel();
        Country country = countryService.getCountryByCode(business.getCountry().getCode());
        State state = stateService.getStateByCodeAndCountry(business.getState().getCode(), country);
        business.setCountry(country);
        business.setState(state);
        business.setUpdatedBy(user);
        business.generateUniqueName();
        business = businessRepository.save(business);
        return new BusinessResource(business);
    }

    @Transactional(readOnly = true)
    public BusinessResource getBusiness(String uniqueName, User user) throws NotFoundException {
        Business business = getBusinessByUniqueNameAndCreator(uniqueName, user);
        return new BusinessResource(business);
    }

    public Business getBusinessByUniqueNameAndCreator(String uniqueName, User user) throws NotFoundException {
        Business business = businessRepository.findByUniqueNameAndCreatedBy(uniqueName, user);
        if(business == null) {
            throw new NotFoundException(messageUtils.t("error.business.notFound"));
        }
        return business;
    }

    @Transactional(readOnly = true)
    public Page<BusinessResource> getPaginatedBusinesses(User user, int page, int count) {
        page = page > 0 ? page - 1 : 0;
        count = count <= 0 ? 5 : count;
        Pageable pageable = PageRequest.of(page, count);
        org.springframework.data.domain.Page<Business> businessPage = businessRepository.findByCreatedBy(user, pageable);
        return Page.createBusinessPage(businessPage);
    }

    @Transactional(readOnly = true)
    public Page<BusinessResource> getPaginatedBusinesses(int page, int count) {
        page = page > 0 ? page - 1 : 0;
        count = count <= 0 ? 5 : count;
        Pageable pageable = PageRequest.of(page, count);
        org.springframework.data.domain.Page<Business> businessPage = businessRepository.findAll(pageable);
        return Page.createBusinessPage(businessPage);
    }
}
