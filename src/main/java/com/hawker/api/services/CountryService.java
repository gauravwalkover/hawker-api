package com.hawker.api.services;

import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.repositories.CountryRepository;
import com.hawker.api.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CountryService {

    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    public Country getCountryByCode(String code) throws NotFoundException {
        Optional<Country> optionalCountry = countryRepository.findByCode(code);
        return optionalCountry.orElseThrow(() -> new NotFoundException("error.country.notFound"));
    }

}
