package com.hawker.api.services;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.ItemCategory;
import com.hawker.api.dao.repositories.ItemCategoryRepository;
import com.hawker.api.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ItemCategoryService {

    private final ItemCategoryRepository itemCategoryRepository;

    public ItemCategoryService(ItemCategoryRepository itemCategoryRepository) {
        this.itemCategoryRepository = itemCategoryRepository;
    }

    public ItemCategory getItemCategoryByCode(String code) throws NotFoundException {
        ItemCategory itemCategory = itemCategoryRepository.findByCode(code);
        if(itemCategory == null) {
            throw new NotFoundException("error.itemCategory.notFound");
        }
        return itemCategory;
    }
}
