package com.hawker.api.services;

import com.hawker.api.dao.models.User;
import com.hawker.api.dao.repositories.UserRepository;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.exceptions.UnAuthorizeException;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.SecurityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

@Service
public class UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserService.class);

    private final UserRepository userRepository;
    private final MessageUtils messageUtils;

    public UserService(UserRepository userRepository, MessageUtils messageUtils) {
        this.userRepository = userRepository;
        this.messageUtils = messageUtils;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User findUserByAuthKey(String authKey) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
            NoSuchAlgorithmException, NoSuchPaddingException, UnAuthorizeException {
        String emailId = SecurityUtils.decryptAuthKeyAndReturnEmailId(authKey);
        Optional<User> optionalUser = userRepository.findByEmailId(emailId);
        return optionalUser.orElseThrow(() -> new UnAuthorizeException(messageUtils.t("error.auth.invalidKey")));
    }

    public User findUserForLoginWithPassword(String emailId, String password) throws UnAuthorizeException {
        password = SecurityUtils.hashPassword(password);
        Optional<User> optionalUser = userRepository.findByEmailIdAndPassword(emailId, password);
        return optionalUser.orElseThrow(() -> new UnAuthorizeException(messageUtils.t("error.login.invalidDetails")));
    }

    public boolean doesEmailIdExist(String emailId) {
        return userRepository.doesEmailIdExist(emailId);
    }

}
