package com.hawker.api.services;

import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.AlreadyExistException;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.resources.UserResource;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.SecurityUtils;
import com.hawker.api.utils.ValidationUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class SignupService {

    private final UserService userService;
    private final MessageUtils messageUtils;

    public SignupService(UserService userService, MessageUtils messageUtils) {
        this.userService = userService;
        this.messageUtils = messageUtils;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public UserResource userSignup(UserResource userResource) throws InvalidParameterException, AlreadyExistException, InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
        ValidationUtils.validateUserSignupRequest(userResource);
        boolean emailIdExists = userService.doesEmailIdExist(userResource.getEmailId());
        if (emailIdExists) {
            throw new AlreadyExistException(String.format(messageUtils.t("error.user.alreadyExist"), userResource.getEmailId()));
        }
        User user = userResource.getModel();
        user.setPassword(SecurityUtils.hashPassword(userResource.getPassword()));
        user.createHashKey();
        user.generateAuthKey();
        user = userService.saveUser(user);
        return new UserResource(user);
    }

}
