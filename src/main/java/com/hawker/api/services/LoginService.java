package com.hawker.api.services;

import com.hawker.api.dao.models.User;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.UnAuthorizeException;
import com.hawker.api.resources.UserResource;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class LoginService {

    private static final Logger LOGGER = LogManager.getLogger(LoginService.class);

    private final UserService userService;
    private final MessageUtils messageUtils;

    public LoginService(UserService userService, MessageUtils messageUtils) {
        this.userService = userService;
        this.messageUtils = messageUtils;
    }

    @Transactional(readOnly = true, rollbackFor = {Throwable.class})
    public UserResource userLogin(UserResource userResource) throws InvalidParameterException, InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException, UnAuthorizeException {
        ValidationUtils.validateUserLoginRequest(userResource);
        User user = userService.findUserForLoginWithPassword(userResource.getEmailId(), userResource.getPassword());
        user.generateAuthKey();
        return new UserResource(user);
    }

}
