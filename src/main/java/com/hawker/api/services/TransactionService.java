package com.hawker.api.services;

import com.hawker.api.dao.models.*;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.utils.MessageUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class TransactionService {

    private final MessageUtils messageUtils;
    private final ItemService itemService;

    public TransactionService(MessageUtils messageUtils, ItemService itemService) {
        this.messageUtils = messageUtils;
        this.itemService = itemService;
    }

    public void validateTransactionsForEntry(Entry entry, Business business) throws NotFoundException, InvalidParameterException {
        List<Transaction> transactions = entry.getTransactions();
        for(Transaction transaction : transactions) {
            Item item = itemService.getItemJoinedByItemPricesByUniqueNameAndBusiness(transaction.getItem().getUniqueName(), business);
            transaction.setItem(item);
            Map<String, ItemUnit> itemUnitMap = item.getItemPrices().stream()
                    .map(ip -> ip.getItemUnit())
                    .collect(Collectors.toMap(iu -> iu.getCode(), Function.identity()));
            if(transaction.getItemUnit() == null || !itemUnitMap.containsKey(transaction.getItemUnit().getCode())) {
                throw new InvalidParameterException(messageUtils.t("error.itemUnit.invalidCode"));
            }
            transaction.setItemUnit(itemUnitMap.get(transaction.getItemUnit().getCode()));
            transaction.setEntry(entry);
        }
    }

}
