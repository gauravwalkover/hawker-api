package com.hawker.api.services;

import com.hawker.api.dao.models.*;
import com.hawker.api.dao.repositories.ItemRepository;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.Page;
import com.hawker.api.resources.ItemResource;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ItemService {

    private static final Logger LOGGER = LogManager.getLogger(ItemService.class);

    private final ItemRepository itemRepository;
    private final ItemCategoryService itemCategoryService;
    private final ItemPriceService itemPriceService;
    private final MessageUtils messageUtils;

    public ItemService(ItemRepository itemRepository, ItemCategoryService itemCategoryService, ItemPriceService itemPriceService,
                       MessageUtils messageUtils) {
        this.itemRepository = itemRepository;
        this.itemCategoryService = itemCategoryService;
        this.itemPriceService = itemPriceService;
        this.messageUtils = messageUtils;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public ItemResource createItem(ItemResource itemResource, Business business, User user)
            throws InvalidParameterException, NotFoundException {
        ValidationUtils.validateCreateItemRequest(itemResource);
        Item item = itemResource.getModel();
        ItemCategory itemCategory = itemCategoryService.getItemCategoryByCode(item.getItemCategory().getCode());
        item.setItemCategory(itemCategory);
        itemPriceService.validateItemPricesForItem(item);
        item.setBusiness(business);
        item.setUpdatedBy(user);
        item.generateUniqueName();
        item = itemRepository.save(item);
        return new ItemResource(item);
    }

    @Transactional(readOnly = true)
    public ItemResource getItem(String uniqueName, Business business) throws NotFoundException {
        Item item = getItemJoinedByItemPricesByUniqueNameAndBusiness(uniqueName, business);
        return new ItemResource(item);
    }

    public Item getItemJoinedByItemPricesByUniqueNameAndBusiness(String uniqueName, Business business) throws NotFoundException {
        Item item = itemRepository.getItemJoinedByItemPricesByUniqueNameAndBusiness(uniqueName, business);
        if(item == null) {
            throw new NotFoundException(messageUtils.t("error.item.notFound"));
        }
        return item;
    }

    @Transactional(readOnly = true)
    public Page<ItemResource> getPaginatedItems(Business business, int page, int count) {
        page = page > 0 ? page - 1 : 0;
        count = count <= 0 ? 5 : count;
        Pageable pageable = PageRequest.of(page, count);
        org.springframework.data.domain.Page<Item> itemPage = itemRepository.getItemPageJoinedByItemPricesByBusiness(business, pageable);
        LOGGER.info(itemPage.getContent().size());
        return Page.createItemPage(itemPage);
    }

    @Transactional(rollbackFor = {Throwable.class})
    public void deleteItem(String uniqueName, Business business) throws NotFoundException {
        Item item = getItemJoinedByItemPricesByUniqueNameAndBusiness(uniqueName, business);
        itemRepository.delete(item);
    }
}
