package com.hawker.api.services;

import com.hawker.api.dao.models.Item;
import com.hawker.api.dao.models.ItemPrice;
import com.hawker.api.dao.models.ItemUnit;
import com.hawker.api.dao.repositories.ItemUnitRepository;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.resources.ItemPriceResource;
import com.hawker.api.utils.MessageUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ItemPriceService {

    private final ItemUnitRepository itemUnitRepository;
    private final MessageUtils messageUtils;

    public ItemPriceService(ItemUnitRepository itemUnitRepository, MessageUtils messageUtils) {
        this.itemUnitRepository = itemUnitRepository;
        this.messageUtils = messageUtils;
    }

    public void validateItemPricesForItem(Item item) throws InvalidParameterException {
        List<ItemUnit> itemUnits = (List<ItemUnit>) itemUnitRepository.findAll();
        Map<String, ItemUnit> itemUnitMap = itemUnits.stream()
                .collect(Collectors.toMap(iu -> iu.getCode(), Function.identity()));
        List<ItemPrice> itemPrices = item.getItemPrices();
        for(ItemPrice itemPrice : itemPrices) {
            if(itemPrice.getItemUnit() == null || !itemUnitMap.containsKey(itemPrice.getItemUnit().getCode())) {
                throw new InvalidParameterException(messageUtils.t("error.itemUnit.invalidCode"));
            }
            itemPrice.setItemUnit(itemUnitMap.get(itemPrice.getItemUnit().getCode()));
            itemPrice.setItem(item);
        }
    }

}
