package com.hawker.api.services;

import com.hawker.api.dao.models.*;
import com.hawker.api.dao.repositories.AccountRepository;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.NotFoundException;
import com.hawker.api.models.AccountBalanceReport;
import com.hawker.api.resources.AccountResource;
import com.hawker.api.utils.MessageUtils;
import com.hawker.api.utils.ValidationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class AccountService {

    private static final Logger LOGGER = LogManager.getLogger(AccountService.class);

    private final AccountRepository accountRepository;
    private final CountryService countryService;
    private final StateService stateService;
    private final MessageUtils messageUtils;

    public AccountService(AccountRepository accountRepository, CountryService countryService, StateService stateService,
                          MessageUtils messageUtils) {
        this.accountRepository = accountRepository;
        this.countryService = countryService;
        this.stateService = stateService;
        this.messageUtils = messageUtils;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public AccountResource createAccount(AccountResource accountResource, Business business, User user)
            throws InvalidParameterException, NotFoundException {
        ValidationUtils.validateCreateAccountRequest(accountResource);
        Account account = accountResource.getModel();
        Country country = account.getCountry() == null ? null : countryService.getCountryByCode(account.getCountry().getCode());
        State state = account.getState() == null ? null : stateService.getStateByCodeAndCountry(account.getState().getCode(), country);
        account.setCountry(country);
        account.setState(state);
        account.setBusiness(business);
        account.setUpdatedBy(user);
        account.generateUniqueName();
        account = accountRepository.save(account);
        return new AccountResource(account);
    }

    @Transactional(readOnly = true)
    public AccountResource getAccount(String accountUniqueName, Business business) throws NotFoundException {
        Account account = getAccountByUniqueNameAndBusiness(accountUniqueName, business);
        return new AccountResource(account);
    }

    public Account getAccountByUniqueNameAndBusiness(String uniqueName, Business business) throws NotFoundException {
        Account account = accountRepository.getAccountByUniqueNameAndBusiness(uniqueName, business);
        if(account == null) {
            throw new NotFoundException(messageUtils.t("error.account.notFound"));
        }
        return account;
    }

    @Transactional(rollbackFor = {Throwable.class})
    public void deleteAccount(String accountUniqueName, Business business) throws NotFoundException {
        Account account = getAccountByUniqueNameAndBusiness(accountUniqueName, business);
        accountRepository.delete(account);
    }

    @Async
    public Future<Map<Long, AccountBalanceReport>> getAsyncAccountBalanceReportIdMapByBusiness(Business business) {
        LOGGER.info("Async call.");
        List<AccountBalanceReport> accountBalanceReports = getAccountDetailsForAccountBalanceReportByBusiness(business);
        return new AsyncResult<>(
                accountBalanceReports.stream()
                        .collect(Collectors.toMap(abr -> abr.getId(), Function.identity()))
        );
    }

    public Map<Long, AccountBalanceReport> getAccountBalanceReportIdMapByBusiness(Business business) {
        List<AccountBalanceReport> accountBalanceReports = getAccountDetailsForAccountBalanceReportByBusiness(business);
        return accountBalanceReports.stream()
                        .collect(Collectors.toMap(abr -> abr.getId(), Function.identity()));
    }

    @Transactional(readOnly = true)
    public List<AccountBalanceReport> getAccountDetailsForAccountBalanceReportByBusiness(Business business) {
        List<AccountBalanceReport> accountBalanceReports = accountRepository
                .getAccountDetailsForAccountBalanceReportByBusiness(business.getId());
        return accountBalanceReports;
    }
}
