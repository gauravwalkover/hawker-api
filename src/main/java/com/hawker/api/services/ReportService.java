package com.hawker.api.services;

import com.hawker.api.dao.models.Business;
import com.hawker.api.dao.models.Entry;
import com.hawker.api.dao.repositories.EntryRepository;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.exceptions.UnexpectedException;
import com.hawker.api.models.AccountBalanceReport;
import com.hawker.api.models.Page;
import com.hawker.api.utils.AsyncUtils;
import com.hawker.api.utils.DateUtils;
import org.joda.time.LocalDate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
public class ReportService {

    private final AccountService accountService;
    private final EntryRepository entryRepository;

    public ReportService(AccountService accountService, EntryRepository entryRepository) {
        this.entryRepository = entryRepository;
        this.accountService = accountService;
    }

    @Transactional(readOnly = true)
    public Page<AccountBalanceReport> generatePaginatedAccountBalanceReport(Business business, String paymentStatus, String from,
                                                                            String to, int page, int count)
            throws InvalidParameterException, UnexpectedException {
        Map<DateUtils.HawkerDate, LocalDate> dateMap = DateUtils.getDatePeriod(from, to);
        LocalDate fromDate = dateMap.get(DateUtils.HawkerDate.FROMDATE);
        LocalDate toDate = dateMap.get(DateUtils.HawkerDate.TODATE);
        Future<Map<Long, AccountBalanceReport>> futureAccountBalanceReportIdMap = accountService.getAsyncAccountBalanceReportIdMapByBusiness(business);
        List<Entry> accountBalances = entryRepository
                .getAccountBalancesByBusinessAndPaymentStatusAndBetweenDates(business.getId(), paymentStatus, fromDate.toDate(), toDate.toDate());
        Map<Long, AccountBalanceReport> accountBalanceReportIdMap = AsyncUtils.getValueOfFutureObject(futureAccountBalanceReportIdMap);
        for(Entry accountBalance : accountBalances) {
            AccountBalanceReport accountBalanceReport = accountBalanceReportIdMap.getOrDefault(accountBalance.getAccount().getId(), new AccountBalanceReport());
            accountBalanceReport.setTotals(accountBalance);
        }
        List<AccountBalanceReport> accountBalanceReports = accountBalanceReportIdMap.values().stream()
                .filter(abr -> abr.getGrandTotal().compareTo(BigDecimal.ZERO) > 0)
                .collect(Collectors.toList());
        Page<AccountBalanceReport> paginatedAccountBalanceReports = Page.getPaginatedResults(accountBalanceReports, page, count);
        return paginatedAccountBalanceReports;
    }
}
