package com.hawker.api.resources;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.dao.models.User;
import com.hawker.api.utils.JsonViews;

public class UserResource extends Resource<User> {

    public UserResource() {
        super(new User());
    }

    public UserResource(User user) {
        super(user);
    }

    public UserResource(String emailId, String password) {
        super(new User(emailId, password));
    }

    public void setEmailId(String emailId) {
        getModel().setEmailId(emailId);
    }

    @JsonView(JsonViews.userEmailId.class)
    public String getEmailId() {
        return getModel().getEmailId();
    }

    public void setName(String name) {
        getModel().setName(name);
    }

    @JsonView(JsonViews.userName.class)
    public String getName() {
        return getModel().getName();
    }

    public void setPassword(String password) {
        getModel().setPassword(password);
    }

    @JsonView(JsonViews.userPassword.class)
    public String getPassword() {
        return getModel().getPassword();
    }

    @JsonView(JsonViews.userAuthKey.class)
    public String getAuthKey() {
        return getModel().getAuthKey();
    }
}
