package com.hawker.api.resources;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.dao.models.ItemCategory;
import com.hawker.api.utils.JsonViews;

public class ItemCategoryResource extends Resource<ItemCategory> {

    public ItemCategoryResource() {
        super(new ItemCategory());
    }

    public ItemCategoryResource(ItemCategory itemCategory) {
        super(itemCategory);
    }

    public void setCode(String code) {
        getModel().setCode(code);
    }

    @JsonView(JsonViews.itemCategoryName.class)
    public String getName() {
        return getModel().getName();
    }

    @JsonView(JsonViews.itemCategoryCode.class)
    public String getCode() {
        return getModel().getCode();
    }

}
