package com.hawker.api.resources;

import com.fasterxml.jackson.annotation.JsonView;
import com.hawker.api.dao.models.Item;
import com.hawker.api.utils.JsonViews;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ItemResource extends Resource<Item> {

    public ItemResource() {
        super(new Item());
    }

    public ItemResource(Item item) {
        super(item);
    }

    public void setItemCategory(ItemCategoryResource itemCategory) {
        getModel().setItemCategory(itemCategory == null ? null : itemCategory.getModel());
    }

    @JsonView(JsonViews.itemCategory.class)
    public ItemCategoryResource getItemCategory() {
        return new ItemCategoryResource(getModel().getItemCategory());
    }

    public void setItemPrices(List<ItemPriceResource> itemPrices) {
        getModel().setItemPrices(CollectionUtils.isEmpty(itemPrices)
                ? new ArrayList<>(0) : itemPrices.stream()
                .map(ip -> ip.getModel())
                .collect(Collectors.toList()));
    }

    @JsonView(JsonViews.itemPrices.class)
    public List<ItemPriceResource> getItemPrices() {
        return getModel().getItemPrices().stream()
                .map(it -> new ItemPriceResource(it))
                .collect(Collectors.toList());
    }

    public void setName(String name) {
        getModel().setName(name);
    }

    @JsonView(JsonViews.itemName.class)
    public String getName() {
        return getModel().getName();
    }

    @JsonView(JsonViews.itemUniqueName.class)
    public String getUniqueName() {
        return getModel().getUniqueName();
    }

    public void setUniqueName(String uniqueName) {
        getModel().setUniqueName(uniqueName);
    }

    public void setDescription(String description) {
        getModel().setDescription(description);
    }

    @JsonView(JsonViews.itemDescription.class)
    public String getDescription() {
        return getModel().getDescription();
    }


}
