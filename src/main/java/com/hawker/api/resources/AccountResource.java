package com.hawker.api.resources;

import com.hawker.api.dao.models.Account;
import com.hawker.api.dao.models.Country;
import com.hawker.api.dao.models.State;

public class AccountResource extends Resource<Account> {

    public AccountResource() {
        super(new Account());
    }

    public AccountResource(Account account) {
        super(account);
    }

    public void setName(String name) {
        getModel().setName(name);
    }

    public String getName() {
        return getModel().getName();
    }

    public String getUniqueName() {
        return getModel().getUniqueName();
    }

    public void setCountry(CountryResource country) {
        getModel().setCountry(country == null ? null : country.getModel());
    }

    public void setState(StateResource state) {
        getModel().setState(state == null ? null : state.getModel());
    }

    public void setAddressLine1(String addressLine1) {
        getModel().setAddressLine1(addressLine1);
    }

    public void setAddressLine2(String addressLine2) {
        getModel().setAddressLine2(addressLine2);
    }

    public void setEmailId(String emailId) {
        getModel().setEmailId(emailId);
    }

    public void setMobileCode(String mobileCode) {
        getModel().setMobileCode(mobileCode);
    }

    public void setMobileNumber(String mobileNumber) {
        getModel().setMobileNumber(mobileNumber);
    }

    public String getEmailId() {
        return getModel().getEmailId();
    }

    public String getMobileCode() {
        return getModel().getMobileCode();
    }

    public String getMobileNumber() {
        return getModel().getMobileNumber();
    }

    public String getAddressLine1() {
        return getModel().getAddressLine1();
    }

    public String getAddressLine2() {
        return getModel().getAddressLine2();
    }

    public StateResource getState() {
        State state = getModel().getState();
        if(state == null) {
            return null;
        }
        return new StateResource(state);
    }

    public CountryResource getCountry() {
        Country country = getModel().getCountry();
        if(country == null) {
            return null;
        }
        return new CountryResource(country);
    }

    public void setType(String type) {
        getModel().setType(type);
    }

    public String getType() {
        return getModel().getType();
    }

}
