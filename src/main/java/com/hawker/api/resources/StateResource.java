package com.hawker.api.resources;

import com.hawker.api.dao.models.State;

public class StateResource extends Resource<State> {

    public StateResource() {
        super(new State());
    }

    public StateResource(State state) {
        super(state);
    }

    public String getName() {
        return getModel().getName();
    }

    public String getCode() {
        return getModel().getCode();
    }

    public void setCode(String code) {
        getModel().setCode(code);
    }

}
