package com.hawker.api.resources;

import com.hawker.api.dao.models.ItemUnit;

public class ItemUnitResource extends Resource<ItemUnit> {

    public ItemUnitResource() {
        super(new ItemUnit());
    }

    public ItemUnitResource(ItemUnit itemUnit) {
        super(itemUnit);
    }

    public void setCode(String code) {
        getModel().setCode(code);
    }

    public String getCode() {
        return getModel().getCode();
    }

    public String getName() {
        return getModel().getName();
    }

}
