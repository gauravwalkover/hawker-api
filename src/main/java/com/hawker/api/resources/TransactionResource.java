package com.hawker.api.resources;

import com.hawker.api.dao.models.Transaction;

import java.math.BigDecimal;

public class TransactionResource extends Resource<Transaction> {

    public TransactionResource() {
        super(new Transaction());
    }

    public TransactionResource(Transaction transaction) {
        super(transaction);
    }

    public void setAmount(BigDecimal amount) {
        getModel().setAmount(amount);
    }

    public void setQuantity(BigDecimal quantity) {
        getModel().setQuantity(quantity);
    }

    public void setItemUnit(ItemUnitResource itemUnit) {
        getModel().setItemUnit(itemUnit.getModel());
    }

    public void setItem(ItemResource item) { getModel().setItem(item.getModel()); }

    public BigDecimal getAmount() {
        return getModel().getAmount();
    }

    public BigDecimal getQuantity() {
        return getModel().getQuantity();
    }

    public ItemResource getItem() {
        return new ItemResource(getModel().getItem());
    }

    public ItemUnitResource getItemUnit() {
        return new ItemUnitResource(getModel().getItemUnit());
    }

}
