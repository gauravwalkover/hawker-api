package com.hawker.api.resources;

import com.hawker.api.dao.models.Business;

public class BusinessResource extends Resource<Business> {

    public BusinessResource() {
        super(new Business());
    }

    public BusinessResource(Business business) {
        super(business);
    }

    public void setCountry(CountryResource country) {
        getModel().setCountry(country == null ? null : country.getModel());
    }

    public void setState(StateResource state) {
        getModel().setState(state == null ? null : state.getModel());
    }

    public void setAddressLine1(String addressLine1) {
        getModel().setAddressLine1(addressLine1);
    }

    public void setAddressLine2(String addressLine2) {
        getModel().setAddressLine2(addressLine2);
    }

    public void setName(String name) {
        getModel().setName(name);
    }

    public void setEmailId(String emailId) {
        getModel().setEmailId(emailId);
    }

    public void setMobileCode(String mobileCode) {
        getModel().setMobileCode(mobileCode);
    }

    public void setMobileNumber(String mobileNumber) {
        getModel().setMobileNumber(mobileNumber);
    }

    public void setTaxNumber(String taxNumber) {
        getModel().setTaxNumber(taxNumber);
    }

    public String getName() {
        return getModel().getName();
    }

    public String getUniqueName() {
        return getModel().getUniqueName();
    }

    public String getTaxNumber() {
        return getModel().getTaxNumber();
    }

    public String getEmailId() {
        return getModel().getEmailId();
    }

    public String getMobileCode() {
        return getModel().getMobileCode();
    }

    public String getMobileNumber() {
        return getModel().getMobileNumber();
    }

    public String getAddressLine1() {
        return getModel().getAddressLine1();
    }

    public String getAddressLine2() {
        return getModel().getAddressLine2();
    }

    public StateResource getState() {
        return new StateResource(getModel().getState());
    }

    public CountryResource getCountry() {
        return new CountryResource(getModel().getCountry());
    }

}
