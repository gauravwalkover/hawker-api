package com.hawker.api.resources;

import com.hawker.api.dao.models.Entry;
import com.hawker.api.exceptions.InvalidParameterException;
import com.hawker.api.models.Regex;
import com.hawker.api.utils.DateUtils;
import org.apache.commons.collections.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EntryResource extends Resource<Entry> {

    public EntryResource() {
        super(new Entry());
    }

    public EntryResource(Entry entry) {
        super(entry);
    }

    public void setEntryDate(String entryDate) throws InvalidParameterException {
        getModel().setEntryDate(DateUtils.parseString(entryDate));
    }

    public String getEntryDate() {
        return getModel().getEntryDate().toString(Regex.DD_MM_YYYY_HYPHEN_SEPARATED);
    }

    public void setTransactions(List<TransactionResource> transactions) {
        getModel().setTransactions(CollectionUtils.isEmpty(transactions)
                ? new ArrayList<>(0) : transactions.stream()
                .map(t -> t.getModel())
                .collect(Collectors.toList()));
    }

    public List<TransactionResource> getTransactions() {
        return getModel().getTransactions().stream()
                .map(t -> new TransactionResource(t))
                .collect(Collectors.toList());
    }

    public void setDescription(String description) {
        getModel().setDescription(description);
    }

    public String getDescription() {
        return getModel().getDescription();
    }

    public void setType(String type) {
        getModel().setType(type);
    }

    public String getType() {
        return getModel().getType();
    }

    public void setPaymentStatus(String paymentStatus) {
        getModel().setPaymentStatus(paymentStatus);
    }

    public String getPaymentStatus() {
        return getModel().getPaymentStatus();
    }

    public BigDecimal getSubTotal() {
        return getModel().getSubTotal();
    }

    public BigDecimal getTaxTotal() {
        return getModel().getTaxTotal();
    }

    public BigDecimal getGrandTotal() {
        return getModel().getGrandTotal();
    }

    public AccountResource getAccount() {
        return new AccountResource(getModel().getAccount());
    }

    public String getUniqueName() {
        return getModel().getUniqueName();
    }

}
