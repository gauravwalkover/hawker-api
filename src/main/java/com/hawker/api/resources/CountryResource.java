package com.hawker.api.resources;

import com.hawker.api.dao.models.Country;

public class CountryResource extends Resource<Country> {

    public CountryResource() {
        super(new Country());
    }

    public CountryResource(Country country) {
        super(country);
    }

    public String getName() {
        return getModel().getName();
    }

    public String getCode() {
        return getModel().getCode();
    }

    public void setCode(String code) { getModel().setCode(code); }

}
