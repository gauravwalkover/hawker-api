package com.hawker.api.resources;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Resource<T> {

    @Getter(onMethod = @__(@JsonIgnore))
    @Setter(value = AccessLevel.PUBLIC)
    private T model;
}
