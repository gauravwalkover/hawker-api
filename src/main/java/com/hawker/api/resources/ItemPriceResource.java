package com.hawker.api.resources;

import com.hawker.api.dao.models.ItemPrice;

import java.math.BigDecimal;

public class ItemPriceResource extends Resource<ItemPrice> {

    public ItemPriceResource() {
        super(new ItemPrice());
    }

    public ItemPriceResource(ItemPrice itemPrice) {
        super(itemPrice);
    }

    public void setItemUnit(ItemUnitResource itemUnit) {
        getModel().setItemUnit(itemUnit.getModel());
    }

    public ItemUnitResource getItemUnit() {
        return new ItemUnitResource(getModel().getItemUnit());
    }

    public void setPrice(BigDecimal price) {
        getModel().setPrice(price);
    }

    public BigDecimal getPrice() {
        return getModel().getPrice();
    }
}
