create table account (

    id bigserial primary key,
    name varchar(100) not null,
    unique_name varchar(20) not null unique,
    business_id bigint not null references business(id),
    type varchar(10) not null check (type in ('CUSTOMER', 'VENDOR', 'BANK', 'CASH')),
    is_active boolean not null default true,
    email_id varchar(255),
    address_line_1 varchar(50),
    address_line_2 varchar(50),
    state_id bigint references state(id),
    country_id bigint references country(id),
    mobile_code varchar(3),
    mobile_number varchar(12),
    created_by bigint not null references users(id),
    updated_by bigint not null references users(id),
    created_at timestamp(6) default current_timestamp,
    updated_at timestamp(6) default current_timestamp

);

create index account_unique_name_index on account(unique_name);
create index account_business_id_index on account(business_id);
create index account_business_id_type_index on account(business_id, type);
create index account_is_active_index on account(is_active) where (is_active = true);