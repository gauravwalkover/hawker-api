create table item_unit (

    id bigserial primary key,
    name varchar(20) not null,
    code varchar(10) not null unique

);

create index item_unit_code_index on item_unit(code);


insert into item_unit (name, code) values ('Kg', 'kg'),
                                          ('Gm', 'gm'),
                                          ('Ltr', 'ltr'),
                                          ('Ml', 'ml'),
                                          ('Nos', 'nos'),
                                          ('Pc', 'pc');