create table item_price (

    id bigserial primary key,
    item_id bigint not null references item(id),
    item_unit_id bigint not null references item_unit(id),
    price numeric(7, 2) not null

);

create index item_price_item_id_index on item_price(item_id);
create unique index item_price_item_id_item_unit_id_unique_index on item_price(item_id, item_unit_id);