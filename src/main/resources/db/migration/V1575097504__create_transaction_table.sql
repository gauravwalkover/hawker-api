create table transaction (

    id bigserial primary key,
    entry_id bigint not null references entry(id),
    item_id bigint not null references item(id),
    item_unit_id bigint not null references item_unit(id),
    amount numeric(7, 2) not null,
    quantity numeric(7, 2) not null

);

create index transaction_entry_id_index on transaction(entry_id);