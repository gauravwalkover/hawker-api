create table business (

    id bigserial primary key,
    name varchar(50) not null,
    unique_name varchar(20) not null unique,
    email_id varchar(255) not null,
    address_line_1 varchar(50) not null,
    address_line_2 varchar(50) not null,
    state_id bigint not null references state(id),
    country_id bigint not null references country(id),
    mobile_code varchar(3) not null,
    mobile_number varchar(12) not null,
    tax_number varchar(15),
    created_by bigint not null references users(id),
    updated_by bigint not null references users(id),
    created_at timestamp(6) default current_timestamp,
    updated_at timestamp(6) default current_timestamp

);

create index business_unique_name_index on business(unique_name);
create index business_name_index on business(name);