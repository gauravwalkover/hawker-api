create table item_category (

    id bigserial primary key,
    name varchar(50) not null,
    code varchar(20) not null unique

);

create index item_category_code_index on item_category(code);


insert into item_category (name, code) values ('Books', 'books');
insert into item_category (name, code) values ('Grocery', 'grocery');