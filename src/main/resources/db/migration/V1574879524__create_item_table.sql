create table item (

    id bigserial primary key,
    name varchar(100) not null,
    unique_name varchar(20) not null unique,
    item_category_id bigint not null references item_category(id),
    description varchar(250),
    business_id bigint not null references business(id),
    created_by bigint not null references users(id),
    updated_by bigint not null references users(id),
    created_at timestamp(6) default current_timestamp,
    updated_at timestamp(6) default current_timestamp

);

create index item_unique_name_index on item(unique_name);
create index item_name_index on item(name);
create index item_item_category_id_index on item(item_category_id);
create index item_business_id_index on item(business_id);