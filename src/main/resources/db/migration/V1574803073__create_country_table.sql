create table country (

    id bigserial primary key,
    name varchar(50) not null,
    code varchar(2) not null unique

);

create index country_code_index on country(code);


insert into country (name, code) values ('India', 'IN');