create table entry (

    id bigserial primary key,
    unique_name varchar(20) not null unique,
    entry_date date not null default current_timestamp,
    description varchar(250),
    account_id bigint not null references account(id),
    business_id bigint not null references business(id),
    type varchar(10) not null check (type in ('SALE', 'PURCHASE')),
    payment_status varchar(20) not null check (payment_status in ('PAID', 'UNPAID', 'CANCELLED', 'RETURNED')),
    sub_total numeric(7, 2) not null,
    tax_total numeric(7, 2) not null,
    grand_total numeric(10, 2) not null,
    created_by bigint not null references users(id),
    updated_by bigint not null references users(id),
    created_at timestamp(6) default current_timestamp,
    updated_at timestamp(6) default current_timestamp

);

create index entry_unique_name_index on entry(unique_name);
create index entry_entry_date_index on entry(entry_date);
create index entry_account_id_index on entry(account_id);
create index entry_business_id_index on entry(business_id);
create index entry_type_index on entry(type);
create index entry_payment_status_index on entry(payment_status);