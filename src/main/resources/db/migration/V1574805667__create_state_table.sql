create table state (

    id bigserial primary key,
    name varchar(50) not null,
    code varchar(2) not null unique,
    country_id bigint not null references country(id)

);

create unique index state_country_id_code_unique_index on state(country_id, code);
create index state_country_id_index on state(country_id);
