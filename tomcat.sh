#! /bin/bash

COMMAND=$1

if [ -z "${COMMAND}" ]; then
    echo "No command provided."
    exit 1
fi

if [ "${COMMAND}" == "deploy" ]; then
    mvn tomcat7:deploy -Dspring.profiles.active=local -DNCRYPT_16=testkeytestkey12
    exit 0
elif [ "${COMMAND}" == "undeploy" ]; then
    mvn tomcat7:undeploy -Dspring.profiles.active=local -DNCRYPT_16=testkeytestkey12
    exit 0
elif [ "${COMMAND}" == "redeploy" ]; then
    mvn tomcat7:redeploy -Dspring.profiles.active=local -DNCRYPT_16=testkeytestkey12
    exit 0
else
    echo "Invalid command " "${COMMAND}" "."
    exit 1
fi
